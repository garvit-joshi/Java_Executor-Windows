# Java_Executor (Windows)
A Simple Java Executor For Making .java Files Compiled And Interpret .class Files.<br>
When I Was New To Java I Had Difficulty in opening Command Prompt, Going To Directory And Then Compiling My Newly Made Files. So, I created this Batch File Program. It simply goes to the directory where the program is being Executed And Gives A List Of .java files that you could compile.<br>
The program works with two Python Files ( Filename_class.py and Filename_java.py ) that I Have included in this Repository.<br>
The project is also available for [Linux User](https://github.com/garvit-joshi/Java_Executor-Linux) <br>

## Pre requisite
1. [Java JDK](https://www.oracle.com/java/technologies/javase-downloads.html).<br>
2. [Python](https://www.python.org/downloads/).<br>
3. Python And Java Should Be Installed With Environment Variables.<br>

## Steps:
1. Install [Python](https://www.python.org/downloads/), when installing Python check the box with "Add Python To PATH".<br>
2. Installl Java
7. Now just Make A Folder In your desired directory where you will keep all your java files And Put These Three Files In The Directory.[Example](Screenshots/3.Files.PNG) <br> <img src =Screenshots/3.Files.PNG width="650" height="350" alt="Files in an java folder"> <br>
8. The Files Are Designed In such a way that they Search For All the .java And .class files and runs them. [Output](Screenshots/4.Output.PNG) <br> <img src =Screenshots/4.Output.PNG width="650" height="350" alt="Files in an java folder"> <br>

### For Notepad++ Support
[Read Support.md](Support.md)
